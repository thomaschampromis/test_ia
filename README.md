Démarche : 
- 3 Relectures du besoin pour bien comprendre le projet 
- Recherche sur la librairie Open CV (article de blog, Documentation officielle) 
- Tutoriel Vidéo pour comprendre les actions de base : https://www.youtube.com/watch?v=LU5R_toaA-E
- J'ai commencé par créér un fichier python tout simple juste pour convertir une image en noir et blanc et l'afficher avec opencv. 
- Ensuite j'ai recherché des articles et vidéos qui expliquent une fonction de opencv pour la reconaissance de forme. J'ai trouvé notamment un article très pertinent sur mon besoin : Tutoriel pour implémenter la function de reconaissance des cercles : https://fr.acervolima.com/detection-de-cercle-avec-opencv-python/

- J'ai créé un petit projet django à l aide de la doc du framework et de projet pro (Scan expertise) pour gagner du temps .J'ai implémenté une page qui affiche un formulaire html avec une fonction dans la views.py qui va traiter les données de ce dernier . 
- J'ai integré la logique de l'agorithme trouvé précedemment pour reconnaitre les cercles. 

- J'ai commencé a intégrer une algo pour les carrés mais par faute de temps je n'ai pas eu le temps de le faire pleinement fonctionner . 



