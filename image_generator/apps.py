from django.apps import AppConfig


class ImageGeneratorConfig(AppConfig):
    name = 'image_generator'
