from contextlib import ContextDecorator
from email.mime import image
from django.http import HttpResponse
from django.shortcuts import redirect, render
import cv2
import requests
from django.conf import settings
import json
import datetime
from django.core.files.base import ContentFile

from django.core.files.base import ContentFile
from django.shortcuts import render
from django.conf import settings
from datetime import datetime
import numpy as np
import base64
from pathlib import Path
from django.core.files import File
    
from django.shortcuts import  render
from django.core.files.storage import FileSystemStorage

from IA_test.settings import BASE_DIR



def upload(request):
    ctx = {}
    if request.method == 'POST' and request.FILES['image']:
        
        # Check file size
        if request.FILES["image"].size > 524880 :
            ctx["error_msg"] = "Fichier trop volumineux , Veuillez charger une autre photo"
            return render(request, 'image_generator/index.html', ctx)

        
        nb_circle = 0
        nb_square = 0
        upload = request.FILES['image']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        file_url = fss.url(file)
        
        
        # ------ FUNCTION SQUARE ------
        img = cv2.imread(f"{BASE_DIR}/{file_url}" , cv2.IMREAD_COLOR)
        
        # Convert to grayscale.
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        # Blur using 3 * 3 kernel.
        gray_blurred = cv2.blur(gray, (3, 3))
        
        # Apply Hough transform on the blurred image.
        detected_circles = cv2.HoughCircles(gray_blurred, cv2.HOUGH_GRADIENT, 1, 20, param1 = 80,param2 = 50, minRadius = 1, maxRadius = 40)
            
        
        # Draw circles that are detected.
        if detected_circles is not None:
        
            # Convert the circle parameters a, b and r to integers.
            detected_circles = np.uint16(np.around(detected_circles))

            for pt in detected_circles[0, :]:

                a, b, r = pt[0], pt[1], pt[2]
        
                nb_circle += 1
                # Draw the circumference of the circle.
                cv2.circle(img, (a, b), r, (255, 255, 0), 2)
        
                # Draw a small circle (of radius 1) to show the center.
                image_final = cv2.circle(img, (a, b), 1, (255, 255, 0), 3)

                myfile = "circle-"+datetime.now().strftime("%Y%m%d-%H%M%S")+".jpg"
                cv2.imwrite(f'{settings.MEDIA_ROOT}/cv2/{myfile}', image_final) #sauvegarder l'image

                path = f'{settings.MEDIA_URL}/cv2/{myfile}'   
                ctx["file_url_circle"] = path
        
        ctx['nb_circle'] = nb_circle

        # ------ FUNCTION SQUARE ------
        
        img = cv2.imread(f"{BASE_DIR}/{file_url}" , cv2.IMREAD_COLOR)
        ret , thrash = cv2.threshold(gray, 240 , 255, cv2.CHAIN_APPROX_NONE)
        contours , hierarchy = cv2.findContours(thrash, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        for contour in contours:
            approx = cv2.approxPolyDP(contour, 0.01* cv2.arcLength(contour, True), True)
            
            x = approx.ravel()[0]
            y = approx.ravel()[1] - 5
            if len(approx) == 4 :
                x, y , w, h = cv2.boundingRect(approx)
                aspectRatio = float(w)/h
                if aspectRatio >= 0.95 and aspectRatio < 1.05:
                    cv2.drawContours(img, [approx], 0, (0, 255, 255), 5)
                    nb_square += 1
                    myfile = "square-"+datetime.now().strftime("%Y%m%d-%H%M%S")+".jpg"
                    cv2.imwrite(f'{settings.MEDIA_ROOT}/cv2/{myfile}', img) #sauvegarder l'image
                    path_circle = f'{settings.MEDIA_URL}/cv2/{myfile}'   
                    ctx["file_url_square"] = path_circle
      
        ctx["nb_square"] = nb_square
                
        return render(request, 'image_generator/index.html', ctx)
    return render(request, 'image_generator/index.html')